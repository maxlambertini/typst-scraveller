#set page(
  "a5",
  numbering: "1",
  margin: (inside: 1.5cm, outside: 1cm)
)

#set text(font: "Accord SF",size:9pt)


#let mayday(maydayText) = box(
  width:100%,
  inset: 1em,
  fill: rgb(255,255,0,100),
  stroke: 3pt+red,  
  align(right , text(tracking: -1pt, weight: "bold", style: "italic", size: 1.5em)[
    #par( maydayText) 
  ])
)

#mayday[
"Mayday,
Mayday...

This is free trader
SCRAPCAT, we
are under
attack...

Main drive is
gone, turret one is
destroyed...

We are losing
pressure fast...

Mayday,
Mayday..."
]

#show par: set block(spacing:0.95em)


= megaScraveller

Uno SCRAPHACK per disperati nello spazio ispirato dal più famoso GDR di fantascienza.

Sei uno SPACE SCRAP che percorre le VIE DELLO SPAZIO per fare FORTUNA! DEPREDA E TRUFFA i ricchi nobili, ESPLORA pianeti
misteriosi, ASSALTA navi corporative e RITIRATI in un PIANETA PARADISO! (o muori provandoci....)

= Crea il tuo SPACE SCRAP

1. Scegli un NOME.
2. Tira 2+1d6 per la Taglia: questo valore indica indica quante sono le tue ferite e quanti oggetti puoi portare.
3. Tira 1d10 per scegliere la tua carriera 

#box(
  [
=== Tabella delle Carriere

#text(8pt)[
 #table(
  columns: (1fr,3fr,1fr,3fr),
  inset: 0.2em,
  stroke:none,
  align: horizon,
[*d10*],
[*Carriera*],
[*d10*],
[*Carriera*],
[1],
[Marine],
[6],
[Scout],
[2],
[Scienziato],
[7],
[Tecnico],
[3],
[Mercante],
[8],
[Contrabbandiere],
[4],
[Mercenario], 
[9],
[Pirata],
[5],
[Pilota],
[10],
[MORTO.],
 )
]
  ])


Se non sei deceduto, aggiungi +1 al valore della carriera.

*RIPETI* questo step della carriera per altre *1d4* volte
e aggiungi ulteriori +1 alla carriera uscita. 

Se in questa fase MUORI, crea un nuovo personaggio e
_repeat_, se non sei morto puoi scegliere di interrompere il giro. 

(esempio: hai fatto 4, completato 3 step e decidi di non rischiare ulteriormente, quindi esci. _Codardo_.)

Scegli poi UNA COSA ( DUE COSE se hai fatto almeno 4 step di carriera) fra:

#box(
  [
#text(8pt)[
 #table(
  columns: (1fr,1fr,1fr,1fr,1fr),
  inset: 0.5em,
  stroke:none,

[*Contatti*],
[*Soldi*],
[*Armi*],
[*Gadget*],
[*Nascondiglio*],
[_"Conosco qualcuno..."_],
[_"Bling! Bling!"_],
[_"Bratatatà!"_],
[_"Se premo qui" -- *KABOOM!*_],
[_"Nessuno ci troverà mai"_],
 )
]
  ])

Se non si vuole rischiare, l'alternativa "sicura" è scegliere 2 carriere e 1 oggetto, senza rischiare la ScrapBuccia.

=== Cose

Ogni *COSA* ha un numero limitato di usi. Tira 1d6, se viene 1 si inceppa, è esaurita, è di conoscenza pubblica o smette di funzionare tra fumi puzzolenti.

=== Per finire...

Tirate tutti *1d20*:  chi fa più basso si ACCOLLA un' Astronave usata e malridotta, con relativo mutuo e le
ultime *4+1d8* rate non pagate e qualcuno o qualcosa di molto arrabbiato alle calcagna.

== REGOLA BASE

Si tirano i dadi se l’esito delle azioni dei personaggi è
in dubbio.

Tira *2d6*, aggiungi *+1* per ogni livello di carriera ap-
plicabile e *+1* per se hai un gadget/arma/nave/
nascondiglio utile.

*Se fai 9+* ottieni un *SUCCESSO!*, riesci a fare quello
che ti proponi di fare.

*Se fai 2* è un *DISASTRO*, tutto va male, le cose si
fanno nere e potresti respirare il vuoto se non stai
attento.

*Se fai 15+ entri in SCRAP TIME:* Sei assolutamente in
fase con l’universo e in quel momento canalizzi l’es-
senza della creazione: consultati col tuo gruppo per
qualsiasi richiesta mistica, brutale, psionica, ecc.
ecc. e il GM ti risponderà.

== COMBATTIMENTO

Fai un tiro regolare, se riesci colpisci, se fallisci vieni
colpito e perdi un punto di Taglia. Se arrivi a 0 ferite
stai morendo, e se qualcuno non ti cura in fretta sei
morto o vai a -1 sei morto. Se sei in SCRAP TIME il
tuo potenziale distruttivo aumenta a dismisura, e
per il round successivo picchierai come un fabbro
ferraio. Narra questa scena nel modo più divertente
possibile.

== L’UNIVERSO E TUTTO QUANTO

Il giocatore con meno carriere descrive un PIANETA
INFERNALE dal quale fuggire. In caso di parità tirare
i dadi, chi fa meno descrive questo pianeta e perché
è così infernale.
Il giocatore con più carriere descrive un PIANETA
PARADISO dove andare in pensione.
Quello con la nave e il debito descrive DA CHI STA
SCAPPANDO. Il gruppo decide IL NOME DELLA NAVE.
Ora preparati e...

= Scraver's Guide

=== Nomi a caso (1d20)

#text(8pt)[
  #table(
    columns: (1fr,1fr,1fr,1fr),
    inset: 0.2em,
    stroke: none,
    [ 1 - Alexei], 
    [ 2 - Alina], 
    [ 3 - Anna], 
    [ 5 - Brooklyn], 
    [ 4 - Arianna], 
    [ 6 - Daiki], 
    [ 7 - Davide], 
    [ 8 - Franziska], 
    [ 9 - Gabriel], 
    [ 10 - Guo], 
    [ 11 - James], 
    [ 12 - Javier], 
    [ 13 - Johanna], 
    [ 14 - Li], 
    [ 15 - Stella], 
    [ 16 - Sydney], 
    [ 17 - Veronica], 
    [ 18 - Wang], 
    [ 19 - Xue], 
    [ 20 - Zoey], 
  )
]

=== Cognomi a caso (1d20)

#text(8pt)[
  #table(
    columns: (1fr,1fr,1fr,1fr),
    inset: 0.2em,
    stroke: none,
    [ 1 - Archibald], 
    [ 2 - Cazaux], 
    [ 3 - Craven], 
    [ 4 - Dubois], 
    [ 5 - Egerszegy], 
    [ 6 - Farina], 
    [ 7 - Gallo], 
    [ 8 - Galloway], 
    [ 9 - Hollister], 
    [ 10 - Khanna], 
    [ 11 - Landau], 
    [ 12 - Nassar], 
    [ 13 - Nielsen], 
    [ 14 - Pendleton], 
    [ 15 - Pendragon], 
    [ 16 - Pfleger], 
    [ 17 - Poirier], 
    [ 18 - Rutherford], 
    [ 19 - Soerensen], 
    [ 20 - Umran], 
  )
]

== Posti (1d20)

#text(8pt)[
1. *Amanogawa*, un cimitero spaziale orbitante intorno a RSD-3421-4 traformato in un insediamento minerario.
2. *Berwyn-4*, una rosetta di 4 mondi identici intorno ad un gigante gassoso
3. *Colbert Falls*, un pianeta dall'atmosfera spesso molto lisergica.
4. *Il Grande Artiglio di Fordland*, una mano alta 5km ce cerca di catturare i velivoli di passaggio.
5. *Hustisford*, l'accademia più prestigiosa della nobiltà galattica.
6. *Iskander*, una colonia talmente vecchia in un mondo talmente ostile dove i coloni originari si sono adattati in una specie di licantropi xenofobi.
7. *Mayakovsky*, il più rinomato teatro spaziale itinerante, noto per la sua acustica e i prezzi dei biglietti decisamente fuori portata
8. *Narrenschil*, la nave fantasma pirata che si dice aiuti disadattati e diseredati e metta i bastoni tra le ruote dell'ordine costituito.
9. *Nuiqsut*, giganteschi resti orbitanti di un ringworld vecchio di centinaia di migliaia d'anni. Archeologi e predoni sgomitano e a volte collaborano.
10. *Oleshko Prime*: E' un mondo devastato, ma migliaia di enormi stazioni orbitanti ospitano città a cupola dove si trova di tutto e tutte le culture dell'universo ci hanno un piede. Occhio alla borsa, però.
11. *Parkway-443*, il più grande cantiere navale del settore MX-537 e un'enorme zona dedicata all'usato sicuro. _Buyers beware_.
12. *Rimbaud*, il pianeta cimitero avvolto in una nebbia autunnale perenne.
13. *Rising Sun*: nonostante il nome è un inospitale pianeta minerario la cui atmosfera irrespirabile è colorata dai geyser di fuoco caratteristici di questo mondo.
14. *RK-4423-68*: ben poco si sa di questo sistema G3V, se non che ci entra difcilmente ne esce. Imperi e corporations mantengono un sistema di stazioni scientifiche ai suoi margini.
15. *Rosebush*: Ogni senziente ha il suo vizio, e lo trova su Rosebush.
16. *Spencerport*: base scientifica su Spencer's World, mondo al 99% acquatico nei cui fondali si trovano gigantesche rovine basaltiche.
17. *Schuyler-7*, pericoloso mondo di addestramento delle forze imperiali.
18. *Toyohiro*, la velocità del tempo su questo pianeta procede irregolarmente. Atterri giovane, parti sul punto di morte se non stai attento.
19. *Yreka*, un mondo totalmente artificiale in rovina abitato da robot pseudo-senzienti che cercano di mantenerlo funzionante come possono. 
20. *Zwier's Whale*, lo scheletro di un gigantesco essere che si estende per2000km nel vuoto spaziale tra Rasalhague-3 e Rasalhague-4
]

=== Persone (1D20)

#text(8pt)[
1. *Annabelle Gutierrez*, mercenaria implacabile con un debole per i cani Chow-Chow.
2. *Xin-Rizk-553*, robot filosofo sopravvissuto alle guerre del IV Impero.
3. *Yu Winthrop*, reduce da un esperimento genetico, la sua memoria assoluta mostra anche le possibili evoluzioni temporali di una situazione .
4. *Giovanni Hashimoto*, vende e compra informazioni come la frutta. Come quest'ultima, possono essere marce o scadute. _Occhio_.
5. *Xinyi Cah-Ssstairs* è un alieno polipoide, viene da un settore periferico ed è l'astro nascente dello zero-g ball
6.*Willow El-Hajj*, mercante dalla parlantina incredibile, ti venderà qualcosa, questo è sicuro. 
7. *Gianna Thomsen* è l'anchorbeing della olotv imperiale nonché pietra di paragone del bonton galattico. Ma _chi è_ in realtà?
8. *Francesca Radcliffe* è una mutante in fuga, quando è nervosa teleporta sé e una sfera di 6 metri di diametro in un posto casuale.
9. *Yumi Carbone* è una guardia del corpo potenziata coltissima.
10. *Grace Hollister*, ermafrodita mutasessuale nonché umanoide da copertina definitivo. Sotto contratto da sempre, cova la ribellione.
11. *Hu Quan Lindholm*, signore della guerra dei ribelli di Sarkyapoon-7.
12. *Valentina Faraday*, coach dei _Trantor Pillars_, campioni imperiali di zero-gball. Telepate multitasker.
13. *N.Eh.A*, astronave senziente in grado di parassitare le altre navi e assimilarne l'equipaggio vivente e no. Adora una partita a scacchi.
14. *Fabian Valois*, politico in ascesa del Senato Imperiale. Ha il suo prezzo, ma pochi sanno qual è. 
15. *Franziska Balle*, l'umana più vecchia dell'universo, ricorda la Terra-Che-Fu. 
16. *Daniel Ozaki*, si dice che possa riparare qualsiasi astronave.
17. *RA-PHA-ELL J3N53N*, profeta della chiesa del silicio e dei quanti. Disprezza gli esseri di carne e sangue.
18. *A.L.I.C.E*, sciame di insetti di dubbia coscienza che assume la forma dell'interlocutore.
19. *Luigi Ghazali*, giornalista d'assalto che vuole scoprire cosa succede intorno a RK-4423-68, costi quel che costi.
20. *Weiwei Cazaux*, gerarca supremo della Stazione Eiffel di *Oleshko Prime*. Nessuno sa chi sia, di che specie sia e quale sia il suo aspetto.
]